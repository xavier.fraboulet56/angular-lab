# Angular Lab

## Etape 1 : Initialisation du projet Angular

1. Installer et utiliser la CLI Angular pour initialiser le projet

> The Angular CLI is a command-line interface tool that you use to initialize, develop, scaffold, and maintain Angular applications directly from a command shell.
> Angular CLI : https://angular.io/cli

<details>
  <summary>Réponse</summary>
  
`npm install -g @angular/cli`

`ng new clarify-ng --skip-tests --directory .`

```bash
Need to install the following packages:
  @angular/cli
Ok to proceed? (y) y
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? SCSS 
```
</details>

<br />

2. Lancer l'application Angular
> Un browser s'ouvre et affiche l'application Angular créée


<details>
  <summary>Réponse</summary>
  
```bash
npm start
```
</details>


<br />


## Etape 2 : CTRL-C CTRL-V
- Pour accélérer la création de l'app, réaliser les étapes suivantes :

- Coller le code ci-dessous dans `app.component.html`
<details>
  <summary>Code</summary>
  
```HTML

<!-- Toolbar -->
<div class="toolbar" role="banner">
  <img
    width="40"
    alt="Angular Logo"
    src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNTAgMjUwIj4KICAgIDxwYXRoIGZpbGw9IiNERDAwMzEiIGQ9Ik0xMjUgMzBMMzEuOSA2My4ybDE0LjIgMTIzLjFMMTI1IDIzMGw3OC45LTQzLjcgMTQuMi0xMjMuMXoiIC8+CiAgICA8cGF0aCBmaWxsPSIjQzMwMDJGIiBkPSJNMTI1IDMwdjIyLjItLjFWMjMwbDc4LjktNDMuNyAxNC4yLTEyMy4xTDEyNSAzMHoiIC8+CiAgICA8cGF0aCAgZmlsbD0iI0ZGRkZGRiIgZD0iTTEyNSA1Mi4xTDY2LjggMTgyLjZoMjEuN2wxMS43LTI5LjJoNDkuNGwxMS43IDI5LjJIMTgzTDEyNSA1Mi4xem0xNyA4My4zaC0zNGwxNy00MC45IDE3IDQwLjl6IiAvPgogIDwvc3ZnPg=="
  />
  <span>Clarify TT New Generation</span>

</div>

<div class="content" role="main">

  <div class="card-container">
    <div class="card" >
      <h1>Titre 1</h1>
      <p>Description 1</p>
    </div> 
    <div class="card" >
      <h1>Titre 2</h1>
      <p>Description 2</p>
    </div> 
    <div class="card" >
      <h1>Titre 3</h1>
      <p>Description 3</p>
    </div> 
    <div class="card" >
      <h1>Titre 4</h1>
      <p>Description 4</p>
    </div> 
  </div>

</div>
```
</details>

- Coller le code ci-dessous dans `styles.scss`

<details>
  <summary>Code</summary>
  
```css
body {
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
    font-size: 14px;
    color: #333;
    box-sizing: border-box;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    margin: 8px 0;
  }

  p {
    margin: 0;
  }

  .spacer {
    flex: 1;
  }

  .toolbar {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    height: 60px;
    display: flex;
    align-items: center;
    background-color: #1976d2;
    color: white;
    font-weight: 600;
  }

  .content {
    display: flex;
    margin: 82px auto 32px;
    padding: 0 16px;
    max-width: 960px;
    flex-direction: column;
    align-items: center;
  }

  .card-container {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    margin-top: 16px;
  }

  .card {
    all: unset;
    border-radius: 4px;
    border: 1px solid #eee;
    background-color: #fafafa;
    width: 960px;
    margin: 0 8px 16px;
    padding: 16px;
    justify-content: left;
    align-items: center;
    line-height: 24px;
  }

```
</details>

- Coller le code ci-dessous dans `app.component.ts`

<details>
  <summary>Code</summary>
  
```typescript
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  tickets = [
    {
      "title" : "Playlogs marche pas",
      "description" : "L'ingress est KO"
    },
    {
      "title" : "Erreur random dans PDC",
      "description" : "Merci d'analyser !"
    },
    {
      "title" : "NullPointer lors de l'appel à l'OM",
      "description" : "Quittez le navire !"
    },
    {
      "title" : "Test de Julien",
      "description" : "Test de Julien à nouveau"
    }
  ];
}

```
</details>



## Etape 3 : Des tickets en boucle
- Utiliser la directive `*ngFor` pour afficher la liste des tickets présents dans `AppComponent` (variable `tickets`)

> https://guide-angular.wishtack.io/angular/composants/ngfor

<details>
  <summary>Réponse</summary>
  
  <div class="card-container">
    <div class="card" *ngFor="let ticket of tickets">
      <h1>{{ticket.title}}</h1>
      <p>{{ticket.description}}</p>
    </div> 
  </div>
</details>

<br />

## Etape 4 : Avec des composants, c'est mieux ;)
> Components are the main building block for Angular applications. Each component consists of:
> - An HTML template that declares what renders on the page
> - A TypeScript class that defines behavior
> - A CSS selector that defines how the component is used in a template
> - Optionally, CSS styles applied to the template
> https://angular.io/guide/component-overview

- Créer un premier composant qui permettra d'afficher la liste des tickets
    - Composant à créer : `ticket-list` dans le dossier `ticket/components`
    - Reporter le code lié à la liste des tickets dans ce composant
    - Utiliser le nouveau composant dans `app.component.html`

<details>
  <summary>Réponse</summary>
  
`ng g c ticket/components/ticket-list`

Et après, il faut coder...
</details>

<br />

- Créer un deuxième composant qui permettra d'afficher un ticket 
    - Composant à créer : `ticket` dans le dossier `ticket/components`
    - Attention : la variable `tickets` contenant la liste des tickets devra rester dans le composant `ticket-list`. Il faudra donc trouver une solution pour partager les données du parent (`ticket-list`) vers l'enfant (`ticket`) (https://angular.io/guide/inputs-outputs)

## Etape 5 : Il y a Type dans Typescript
La liste des `tickets` est pour l'instant non-typée. Typescript permet de typer les objets via des class par exemple (comme dans Java) : cela a plusieurs [avantages](http://pchiusano.github.io/2016-09-15/static-vs-dynamic.html).
- Créer une class pour représenter un ticket : `ng generate class ticket/models/ticket --type=model`
- Ajouter les champs `title` et `description`
```typescript
export class Ticket {
    public description: string = "";
    public title: string = "";
}
```
- Modifer le composant `TicketListComponent` pour typer la variable `tickets`
```typescript
tickets: Ticket[] = [...]
```

## Etape 6 : Avec un service, c'est encore mieux
> Why services :
>
> Components shouldn't fetch or save data directly and they certainly shouldn't knowingly present fake data. They should focus on presenting data and delegate data access to a service.

- Créer un nouveau service nommé `ticket` dans le dossier `ticket/services`
    - En principe, ce service devrait appeler une API via le [HttpClient d'angular](https://angular.io/guide/http). Cependant, pour ce lab, les tickets seront stockés dans une variable présente dans le service.
    - Coller la variable `tickets` de `TicketListComponent` dans ce service
    - Le service offrira la méthode `getTickets` qui retournera la variable `tickets`
    - Dans `TicketListComponent`, appeler la méthode `getTickets` pour valoriser `tickets`
    - Exemple d'utilisation : https://angular.io/tutorial/toh-pt4

<details>
  <summary>Réponse</summary>
  
```bash
ng g s ticket/services/ticket
```

- `ticket.service.ts` :
```typescript
import { Injectable } from '@angular/core';
import { Ticket } from '../models/ticket.model';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  tickets: Ticket[] = [
    {
      "title" : "Playlogs marche pas",
      "description" : "L'ingress est KO"
    },
    {
      "title" : "Erreur random dans PDC",
      "description" : "Merci d'analyser !"
    },
    {
      "title" : "NullPointer lors de l'appel à l'OM",
      "description" : "Quittez le navire !"
    },
    {
      "title" : "Test de Julien",
      "description" : "Test de Julien à nouveau"
    }
  ];

  constructor() { }

  public getTickets(): Ticket[] {
    return this.tickets;
  }
}
```

- `ticket-list.component.ts`
```typescript
import { Component, OnInit } from '@angular/core';
import { Ticket } from '../../models/ticket.model';
import { TicketService } from '../../services/ticket.service';

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.scss']
})
export class TicketListComponent implements OnInit {

  tickets: Ticket[] = [];

  constructor(private ticketService: TicketService) { }

  ngOnInit(): void {
    this.tickets = this.ticketService.getTickets();
  }

}
```

</details>

## Etape 7 : La pluie de tickets
Nous allons maintenant permettre d'ajouter de nouveaux tickets.

- Ajouter le module Angular `FormsModule` : cf. https://angular.io/tutorial/toh-pt1#the-missing-formsmodule 
- Créer un composant `ticket-form` dans le dossier `ticket/components`
- Créer un formulaire permettant d'ajouter des tickets 
  - Dans `ticket-form.component.ts`, ajouter une variable `ticket`
  - Dans `ticket-form.component.html`, construire un formulaire HTML qui contiendra les inputs `title` et `description`. Ils seront bindés avec la variable `ticket` via la directive `ngModel`. Exemple : https://angular.io/tutorial/toh-pt1#edit-the-hero
  - Pour comprendre le fonctionnement de `ngModel`, afficher la valeur des deux inputs en dessous du formulaire (via `{{ticket.title}}`)
- Ajouter un bouton "Add"
  - Modifier le service `TickerService` pour ajouter la méthode `add` permettant d'ajouter un ticket dans la liste `tickets`
  - Ajouter un bouton `ticket-form.component.html` dans permettant d'appeler la méthode `add` du service `TickerService`
  - https://angular.io/guide/event-binding
